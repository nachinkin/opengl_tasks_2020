add_compile_definitions(GLM_ENABLE_EXPERIMENTAL)

set(SRC_FILES
        common/Application.cpp
        common/DebugOutput.cpp
        common/Camera.cpp
        common/ConditionalRender.cpp
        common/Framebuffer.cpp
        common/Mesh.cpp
        common/QueryObject.cpp
        common/ShaderProgram.cpp
        common/SkinnedMesh.cpp
        common/Texture.cpp
        common/Utils.cpp
        main.cpp
        )

set(CMAKE_CXX_STANDARD 17)


include_directories(common)

MAKE_OPENGL_TASK(696nachinkin 2 "${SRC_FILES}")
