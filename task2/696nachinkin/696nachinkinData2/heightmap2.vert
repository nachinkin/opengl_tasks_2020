/**
Преобразует координаты вершины и нормально в систему координат виртуальной камеры и передает на выход.
Копирует на выход текстурные координаты.
*/

#version 330

struct LightInfo
{
    vec3 dir; // положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
    vec3 La; // цвет и интенсивность окружающего света
    vec3 Ld; // цвет и интенсивность диффузного света
    vec3 Ls; // цвет и интенсивность бликового света
};


uniform LightInfo light;

// стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

// матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 normalToCameraMatrix;


layout(location = 0) in vec3 vertexPosition; // координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; // нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; // текстурные координаты вершины


//out vec3 normalCamSpace; // нормаль в системе координат камеры
//out vec4 posCamSpace; // координаты вершины в системе координат камеры
out vec2 texCoord; // текстурные координаты
out vec3 lightColor; // Освещенность
out vec3 blinn_vec;
out float height; // текущая высота


const float shininess = 128.0;

void main()
{
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);

    // Out1
    texCoord = vertexTexCoord;

	vec4 posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0); //преобразование координат вершины в систему координат камеры
	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
	//vec3 lightDirCamSpace = normalize(light.dir - posCamSpace.xyz); //направление на источник света

    // For directional light
    vec3 normalCamSpace = normalize(normalToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камеры
    vec4 lightDirCamSpace = viewMatrix * normalize(vec4(light.dir, 0.0));
    float NdotL = max(dot(normalCamSpace, lightDirCamSpace.xyz), 0.0);


	vec3 blinn_vec = vec3(0.0, 0.0, 0.0);
	if (NdotL > 0.0)
	{			
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света
		float blinnTerm = max(dot(normalCamSpace, halfVector), 0.0); //интенсивность бликового освещения по Блинну				
		blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		blinn_vec = light.Ls * blinnTerm;
	}
	

    // Out2
    lightColor = (light.La + light.Ld * NdotL);

    // Out3
    height = vertexPosition[2];
}
