#include <heights_map.h>
#include <grid_terrain.h>
#include <Application.hpp>
#include <Mesh.hpp>
#include <LightInfo.hpp>
#include <ShaderProgram.hpp>
#include "common/Texture.hpp"
#include "common/texture_material.h"

#include <glm/gtc/matrix_transform.hpp>

#include <glm/gtx/transform.hpp>
#include <iostream>
#include <vector>

/**
Несколько примеров шейдеров
*/
class SampleApplication : public Application
{
public:
    // CameraMovers
    std::shared_ptr<TerrainCameraMover> TerrainCameraMover_ = std::make_shared<TerrainCameraMover>();
    std::shared_ptr<FirstPersonCameraMover> FirstPersonCameraMover_ = std::make_shared<FirstPersonCameraMover>();
    std::vector<CameraMoverPtr> CameraMovers_ = {
        FirstPersonCameraMover_,
        TerrainCameraMover_,
    };
    int CurrentCameraMoverIndex = 0;

    // Shaders
    std::vector<ShaderProgramPtr> Shaders_;
    int CurrentShaderIndex_ = 1;
    ShaderProgramPtr SkyBoxShader_ = nullptr;

    // Mesh
    MeshPtr TerrainMesh_;
    MeshPtr BackgroundCube_;

    float HeightScale_ = 0.3f;
    float SizeScale_ = 10.f;
    float MovementSpeed_ = 2.f;

    HeightsMap HeightsMap_;
    GridTerrain GridTerrain_;

    LightInfo Light_;

    float Lr_ = 10.0;
    float Phi_ = glm::pi<float>() / 2;
    float Theta_ = 0;

    float WaterLevel_ = 0.01;

    TextureInfoPtr GrassTexture_;
	TextureInfoPtr SandTexture_;
    TextureInfoPtr RockTexture_;
    TextureInfoPtr SnowTexture_;
    TextureInfoPtr WaterTexture_;

    TexturePtr _cubeTex;
    GLuint _cubeTexSampler;
    const GLuint CubeUnit_ = 0;

    SampleApplication()
        : Application(std::make_unique<TerrainCameraMover>())
        , HeightsMap_("696nachinkinData3/ireland.png")
        , GridTerrain_(HeightsMap_)
    {
        std::cerr << HeightsMap_.GetHeight() << " " << HeightsMap_.GetWidth() << std::endl;
        FirstPersonCameraMover_->ResetModel(&HeightsMap_, SizeScale_);
    }

    void makeScene() override
    {
        Application::makeScene();

        TerrainMesh_ = GridTerrain_.GetMesh();

        UpdateModelMatrix();

        Shaders_.push_back(std::make_shared<ShaderProgram>(
            "696nachinkinData3/heightmap1.vert",
            "696nachinkinData3/heightmap1.frag"
        ));
        Shaders_.push_back(std::make_shared<ShaderProgram>(
            "696nachinkinData3/heightmap2.vert",
            "696nachinkinData3/heightmap2.frag"
        ));

        SkyBoxShader_ = std::make_shared<ShaderProgram>(
            "696nachinkinData3/skybox.vert",
			"696nachinkinData3/skybox.frag"
        );

        _cubeTex = loadCubeTexture("696nachinkinData3/IrelandSkybox");
        BackgroundCube_ = makeCube(10.f);

        glGenSamplers(1, &_cubeTexSampler);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);


        //Инициализация значений переменных освщения
        Light_.ambient = glm::vec3(0.2f, 0.2f, 0.2f);
        Light_.diffuse = glm::vec3(0.8f, 0.8f, 0.8f);
        Light_.specular = glm::vec3(1.f, 1.f, 1.f);


		//=========================================================
		//Загрузка и создание текстур
		WaterTexture_ = std::make_shared<TextureMaterial>(
			"696nachinkinData3/textures/water.jpg", "water",
			1, 0.1, 0.05
			);

		SandTexture_ = std::make_shared<TextureMaterial>(
			"696nachinkindata3/textures/sand_2.jpg", "sand",
			0, 0.05, 0.05
			);

		GrassTexture_ = std::make_shared<TextureMaterial>(
			"696nachinkinData3/textures/rock.jpg", "rock",
			0, 0.5, 0.05
			);


		RockTexture_ = std::make_shared<TextureMaterial>(
			"696nachinkinData3/textures/grass.jpg", "grass",
			0., 0.5, 0.05
			);
		SnowTexture_ = std::make_shared<TextureMaterial>(
			"696nachinkinData3/textures/snow.png", "snow",
			0.9, 0.85, 0.01
			);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
            ImGui::Text("scale %.4f", HeightScale_);
            ImGui::SliderFloat("HeightScale", &HeightScale_, 0.05f, 2.f);
            ImGui::SliderFloat("SizeScale", &SizeScale_, 1.f, 20.f);
            ImGui::SliderFloat("WaterLevel", &WaterLevel_, 0.f, 1.f);
            ImGui::SliderFloat("Move speed", &MovementSpeed_, 0.001f, 2.f);
            ImGui::RadioButton("Terrain camera", &CurrentCameraMoverIndex, 0);
            ImGui::RadioButton("Player camera", &CurrentCameraMoverIndex, 1);


            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(Light_.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(Light_.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(Light_.specular));


                ImGui::SliderFloat("r", &Lr_, 0.1f, 50.f);
                ImGui::SliderFloat("phi", &Phi_, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &Theta_, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void UpdateModelMatrix() {

        float scale = SizeScale_;
        glm::tmat4x4<float> translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-scale / 2, -scale / 2, 0.0f));

        glm::vec3 scalingVector = glm::vec3(scale, scale, HeightScale_);
        glm::tmat4x4<float> scalingMatrix = glm::scale(scalingVector);
        glm::tmat4x4<float> modelMatrix = translationMatrix /* rotationMatrix*/ * scalingMatrix;

        TerrainMesh_->setModelMatrix(modelMatrix);
    }

	void drawSkyBox(const ShaderProgramPtr& SkyBoxShader_, const CameraInfo& Camera_) {
		SkyBoxShader_->use();

		glm::vec3 cameraPos = glm::vec3(glm::inverse(Camera_.viewMatrix)[3]); //Извлекаем из матрицы вида положение виртуальный камеры в мировой системе координат

		SkyBoxShader_->setVec3Uniform("cameraPos", cameraPos);
		SkyBoxShader_->setMat4Uniform("viewMatrix", Camera_.viewMatrix);
		SkyBoxShader_->setMat4Uniform("projectionMatrix", Camera_.projMatrix);
		SkyBoxShader_->setFloatUniform("coef", 1.f);

		//Для преобразования координат в текстурные координаты нужна специальная матрица
		glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		SkyBoxShader_->setMat3Uniform("textureMatrix", textureMatrix);

		if (USE_DSA) {
			glBindTextureUnit(CubeUnit_, _cubeTex->texture());
			glBindSampler(CubeUnit_, _cubeTexSampler);
		}
		else {
			glActiveTexture(GL_TEXTURE0 + CubeUnit_);
			glBindSampler(CubeUnit_, _cubeTexSampler);
		}

		_cubeTex->bind();
		SkyBoxShader_->setIntUniform("cubeTex", 0);

		glDepthMask(GL_FALSE); //Отключаем запись в буфер глубины

		BackgroundCube_->draw();

		glDepthMask(GL_TRUE); //Включаем обратно запись в буфер глубины
	}

    void draw() override
    {
        FirstPersonCameraMover_->ResetScale(SizeScale_);
        ResetCameraMover(CameraMovers_.at(CurrentCameraMoverIndex));
        CameraMover_->SetMovementSpeed(MovementSpeed_);
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        UpdateModelMatrix();

		drawSkyBox(SkyBoxShader_, Camera_);

        // шейдер
        assert(0 <= CurrentShaderIndex_ && CurrentShaderIndex_ < Shaders_.size());
        ShaderProgramPtr shader = Shaders_.at(CurrentShaderIndex_);
        assert(shader);
        shader->use();

        shader->setFloatUniform("WaterLevel", WaterLevel_);

        // Загружаем на видеокарту значения юниформ-переменных
        shader->setMat4Uniform("viewMatrix", Camera_.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", Camera_.projMatrix);
        shader->setMat3Uniform(
            "normalToCameraMatrix",
            glm::transpose(
                glm::inverse(
                    glm::mat3(Camera_.viewMatrix  * TerrainMesh_->GetModelMatrix())
                )
            )
        );

        // Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        assert(TerrainMesh_ != nullptr);
        shader->setMat4Uniform("modelMatrix", TerrainMesh_->modelMatrix());


        // Свет
        glm::vec3 lightDir = glm::vec3(
            glm::cos(Phi_) * glm::cos(Theta_),
            glm::sin(Phi_) * glm::cos(Theta_),
            glm::sin(Theta_)
        );


        shader->setVec3Uniform("light.dir", lightDir); //копируем положение уже в системе виртуальной камеры
        shader->setVec3Uniform("light.La", Light_.ambient);
        shader->setVec3Uniform("light.Ld", Light_.diffuse);
        shader->setVec3Uniform("light.Ls", Light_.specular);

		WaterTexture_->bind(shader, 1);
		SandTexture_->bind(shader, 2);
		GrassTexture_->bind(shader, 3);
		RockTexture_->bind(shader, 4);
		SnowTexture_->bind(shader, 5);

        TerrainMesh_->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
