#version 330

uniform samplerCube cubeTex;
uniform float coef;

in vec3 texCoord; //текстурные координаты (интерполированы между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента
layout(location = 0) out vec4 colorTex;

void main()
{
	vec3 texColor = texture(cubeTex, texCoord).rgb;

	fragColor = coef * vec4(texColor, 1.0);
	colorTex = fragColor;
}
