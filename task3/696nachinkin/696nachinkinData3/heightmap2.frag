/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

struct TextureMaterial
{
    float Ka;
    float Kd;
    float Ks;  //Коэффициент бликового отражения
    float Height;
    sampler2D DiffuseTex;
};


uniform TextureMaterial water;
uniform TextureMaterial grass;
uniform TextureMaterial rock;
uniform TextureMaterial snow;
uniform TextureMaterial sand;
uniform float WaterLevel;

//in vec3 normalCamSpace; // нормаль в системе координат камеры
//in vec4 posCamSpace; // координаты вершины в системе координат камеры
in vec2 texCoord; // текстурные координаты (интерполирована между вершинами треугольника)
in vec3 lightColor; // Освещенность (интерполирована между вершинами треугольника)
in vec3 blinn_vec;
in float height; // текущая высота (интерполирована между вершинами треугольника)

out vec4 fragColor; // выходной цвет фрагмента

const float sigma = 0.7;

float norm_coef(float x, float mu, float sigma_) {
    return exp(-((x - mu) * (x - mu) / (2 * sigma_*sigma_)));
}

void main()
{
    vec3 diffuseColor =
	    + texture(sand.DiffuseTex, texCoord).rgb * norm_coef(height, 0.05, sigma/20)
        + texture(grass.DiffuseTex, texCoord).rgb * norm_coef(height, 2*sigma/7, sigma/7)
        + texture(rock.DiffuseTex, texCoord).rgb * norm_coef(height, 4*sigma/7, sigma/7)
        + texture(snow.DiffuseTex, texCoord).rgb * norm_coef(height, 0.85, 3 * sigma/7);

	vec3 Ks = vec3(1.0, 1.0, 1.0) * 100*(norm_coef(height, 0.05, sigma/20)*sand.Ks
								 + norm_coef(height, 2*sigma/7, sigma/7)*grass.Ks
								 + norm_coef(height, 5*sigma/7, sigma/7)*rock.Ks
								 + norm_coef(height, 0.85, 3 * sigma/7)*snow.Ks);

    if (height < WaterLevel) {
        diffuseColor = texture(water.DiffuseTex, texCoord).rgb * norm_coef(height, 0, WaterLevel / 2);
		Ks = vec3(0.0, 0.0, 0.0);
    }


	vec3 color = diffuseColor * lightColor + blinn_vec * Ks;

	fragColor = vec4(color, 1.0);
}
 