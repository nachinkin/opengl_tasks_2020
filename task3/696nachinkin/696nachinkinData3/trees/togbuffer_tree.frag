#version 330

uniform sampler2D LeavesTexture;
uniform sampler2D BarkTexture;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

layout(location = 0) out vec3 normal; // "go to GL_COLOR_ATTACHMENT0"
layout(location = 1) out vec3 diffuseColor; // "go to GL_COLOR_ATTACHMENT1"

void main()
{
	vec2 newTexCoord = texCoord;
	newTexCoord.y = newTexCoord.y * 2;
	if (texCoord.y < 0.5){
		diffuseColor = texture(BarkTexture, newTexCoord).rgb;
	} else {
		diffuseColor = texture(LeavesTexture, newTexCoord).rgb;
	}

	normal = normalize(normalCamSpace) * 0.5 + 0.5;
}
