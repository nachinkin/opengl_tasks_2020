#pragma once

#include "ShaderProgram.hpp"
#include "Texture.hpp"

#include <string>
#include <filesystem>

class TextureMaterial {
    TexturePtr Texture_ = nullptr;
    const std::string ShaderName_{};

    GLuint Sampler_{};

    GLfloat TextureHeightLoc_ = 0.5f;

    GLfloat Ka_ = 0;
    GLfloat Kd_ = 0;
    GLfloat Ks_;

public:
    TextureMaterial(
        const std::string &path,
        std::string shaderName,
        float ks = 1,
        float textureHeightLoc = 0.1,
        float textureHeightSigmaSqr = 0.1f * 0.1f
    )
        : ShaderName_(std::move(shaderName))
        , Ks_(ks)
    {
        if (!std::filesystem::exists(path)) {
            throw std::runtime_error(std::string(__FUNCTION__) + ": '" + std::string(path) + "' does not exist");
        }
        Texture_ = loadTexture(path);
        assert(Texture_);
        TextureHeightLoc_ = textureHeightLoc;

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &Sampler_);
        glSamplerParameteri(Sampler_, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(Sampler_, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(Sampler_, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(Sampler_, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glSamplerParameterf(Sampler_, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
    }

    void bind(ShaderProgramPtr shader, unsigned int index) {

        GLuint textureUnitForDiffuseTex = index;
        if (USE_DSA) {
            glBindTextureUnit(textureUnitForDiffuseTex, Texture_->texture());
            glBindSampler(textureUnitForDiffuseTex, Sampler_);
        }
        else {
            glBindSampler(textureUnitForDiffuseTex, Sampler_);
            glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);
        }

//        shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);
//
        shader->setIntUniform(ShaderName_ + ".DiffuseTex", textureUnitForDiffuseTex);
        shader->setFloatUniform(ShaderName_ + ".Height", TextureHeightLoc_);
        shader->setFloatUniform(ShaderName_ + ".Ka", Ka_);
        shader->setFloatUniform(ShaderName_ + ".Kd", Kd_);
        shader->setFloatUniform(ShaderName_ + ".Ks", Ks_);
    }
};

using TextureInfoPtr = std::shared_ptr<TextureMaterial>;
